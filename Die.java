import java.util.Random;

public class Die{
  private int pips;
  private Random random;
  
  public Die(){
    this.pips = 1;
    this.random = new Random();
  }
  public int getPips(){
    return this.pips;
  }
  public Random getRandom(){
    return this.random;
  }
  //(6)+1 allows for values from 1-6 to be rolled
  public int getRoll(){
    return random.nextInt(6)+1;
  }
  //priting the pips on each die object
  public String toString() {
    return "Pips: " + getRoll();
  }
}