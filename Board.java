public class Board{
  private Die die1;
  private Die die2;
  private boolean[] closedTiles;
  
  //the contsructor 
  public Board(){
    this.die1 = new Die();
    this.die2 = new Die();
    this.closedTiles = new boolean[12];
  }
  public String toString() {
    String numbersLeft = "";
    for (int i=0; i<closedTiles.length; i++){
      if (closedTiles[i] == true){ 
        //the number that was returned false in the plyATurn method is replaced with an X
        numbersLeft += "X "; 
      }
      else{ 
        //first iteration is set to false (all numbers) because of all the values in the array are false
        numbersLeft += (i+1+ " ");
      }
    }
    return numbersLeft;
  }
  //instance method that calculates the sum of the die roll and returns whether to put down the tile or not
  public boolean playATurn(){
    int roll1 = die1.getRoll();
    int roll2 = die2.getRoll();
    System.out.println ("Die 1 rolled a: " +roll1);
    System.out.println ("Die 2 rolled a: " +roll2);
    int newRoll = roll1+roll2; 
    
    //[newroll-1] because the roll has to match the position in the array, because there it is 0 indexed, you have to subtract 1 from the position
    if ((closedTiles[newRoll-1] == false)){ 
      this.closedTiles[newRoll-1] = true;
      System.out.println("Closing the tile " +(newRoll)); 
      return false;
    }else{
      System.out.println("This tile is already shut");
      return true; 
    }
  }
}





